class Test2Controller < UIViewController
  def init
    super
    self.tabBarItem = UITabBarItem.alloc.initWithTitle "Tab", image: nil, tag: 0
    self
  end

  def viewDidLoad
    super

    parentViewController.title = 'motion-layout'
    @items = (0..50).to_a

    @table = UITableView.alloc.initWithFrame(self.view.bounds)
    @table.dataSource = self

    Motion::Layout.new do |l|
      l.view view
      l.subviews table: @table
      l.vertical "|-55-[table]-50-|"
      l.horizontal "|[table]|"
    end
  end

  def tableView(tableView, numberOfRowsInSection:section)
    @items.size
  end

  CellID = 'CellIdentifier'
  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell = tableView.dequeueReusableCellWithIdentifier(CellID) || UITableViewCell.alloc.initWithStyle(UITableViewCellStyleSubtitle, reuseIdentifier:CellID)
    item = @items[indexPath.row]
    cell.textLabel.text = item.to_s
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator
    cell
  end
end
