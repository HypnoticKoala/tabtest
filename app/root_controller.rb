class RootController < UITableViewController
  def viewDidLoad
    super

    navigationItem.title = 'My App'
    @items = [
      'UITableViewController',
      'motion-layout'
    ]
  end

  def tableView(tableView, numberOfRowsInSection:section)
    @items.size
  end

  CellID = 'CellIdentifier'
  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell = tableView.dequeueReusableCellWithIdentifier(CellID) || UITableViewCell.alloc.initWithStyle(UITableViewCellStyleSubtitle, reuseIdentifier:CellID)
    item = @items[indexPath.row]
    cell.textLabel.text = item.to_s
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator
    cell
  end

  def tableView(tableView, didSelectRowAtIndexPath:indexPath)
    item = @items[indexPath.row]

    case item
    when 'UITableViewController'
      @detail_controller = UITabBarController.alloc.init
      @detail_controller.setViewControllers([
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init,
        TestController.alloc.init
      ])
    when 'motion-layout'
      @detail_controller = UITabBarController.alloc.init
      @detail_controller.setViewControllers([
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init,
        Test2Controller.alloc.init
      ])
    end

    self.navigationController.pushViewController(@detail_controller, animated:true)
  end
end
