class TestController < UITableViewController
  def init
    super
    self.tabBarItem = UITabBarItem.alloc.initWithTitle "Tab", image: nil, tag: 0
    self
  end


  def viewDidLoad
    super

    self.parentViewController.navigationItem.title = 'UITableViewController'
    @items = (0..50).to_a
  end

  def tableView(tableView, numberOfRowsInSection:section)
    @items.size
  end

  CellID = 'CellIdentifier'
  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell = tableView.dequeueReusableCellWithIdentifier(CellID) || UITableViewCell.alloc.initWithStyle(UITableViewCellStyleSubtitle, reuseIdentifier:CellID)
    item = @items[indexPath.row]
    cell.textLabel.text = item.to_s
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator
    cell
  end
end
